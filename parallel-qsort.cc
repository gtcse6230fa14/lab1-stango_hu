/**
 *  \file parallel-qsort.cc
 *
 *  \brief Implement your parallel quicksort algorithm in this file.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "sort.hh"
#include <pthread.h>

#define TUNING 1024
#define PAR_TUNING 1000000
#define PARALLEL_PARTITION_PREFIXSUM false

/**
 *  Given a pivot value, this routine partitions a given input array
 *  into two sets: the set A_le, which consists of all elements less
 *  than or equal to the pivot, and the set A_gt, which consists of
 *  all elements strictly greater than the pivot.
 *
 *  This routine overwrites the original input array with the
 *  partitioned output. It also returns the index n_le such that
 *  (A[0:(k-1)] == A_le) and (A[k:(N-1)] == A_gt).
 */
int partition (keytype pivot, int N, keytype* A, int* less, int* great, keytype* B)
{
  if (N <= PAR_TUNING) {
    int k = 0;
    for (int i = 0; i < N; ++i) {
    const int ai = A[i];
      if (ai <= pivot) {
        int ak = A[k];
        A[k++] = ai;
        A[i] = ak;
      }
    }
    return k;
  }

  _Cilk_for (int i = 0; i < N; i ++) {
     if (A[i] <= pivot) {
	less[i] = 1;
	great[i] = 0;
     } else {
        great[i] = 1;
        less[i] = 0;
     }
     B[i] = A[i];
  } 

  _Cilk_spawn par_prefixsum(N, less);
  par_prefixsum(N, great);
  _Cilk_sync;
  
  int k = less[N - 1];
  _Cilk_for (int i = 0; i < N; ++i) {
    if (B[i] <= pivot) {
       A[less[i] - 1] = B[i];
    } else {
       A[k + great[i] - 1] = B[i];
    }
  }
  
  return k;
}

void
par_prefixsum(int N, int* A) {
  recursivePrefixSum(N, A);
}

int recursivePrefixSum(int N, int *A) {
  if (N <= TUNING) {
    for (int i = 1; i < N; i ++) A[i] += A[i - 1];
    return A[N - 1];
  }

  _Cilk_spawn recursivePrefixSum(N/2, A);
  recursivePrefixSum(N - N/2, &A[N/2]);
  _Cilk_sync;
  int x = A[N / 2 - 1];
  _Cilk_for (int i = N / 2; i < N; i ++) {
    A[i] += x;
  }
  return A[N - 1];
}

void
quickSort (int N, keytype* A, int* pointers, int* rpointers, keytype* B)
{
  const int G = 1024; /* base case size, a tuning parameter */
  if (N < G)
    sequentialSort (N, A);
  else {
    // Choose pivot at random
    keytype pivot = A[rand () % N];

    // Partition around the pivot. Upon completion, n_less, n_equal,
    // and n_greater should each be the number of keys less than,
    // equal to, or greater than the pivot, respectively. Moreover, the array
    int n_le = partition (pivot, N, A, pointers, rpointers, B);
    _Cilk_spawn quickSort (n_le, A, pointers, rpointers, B);
    quickSort (N-n_le, A + n_le, pointers + n_le, rpointers + n_le, B + n_le);
    _Cilk_sync;
  }
}

int ParPartition (keytype pivot, int N, keytype* A)
{
  if (N < PAR_TUNING) {
    int k = 0;
    for (int i = 0; i < N; ++i) {
      const int ai = A[i];
      if (ai <= pivot) {
        /* Swap A[i] and A[k] */
        int ak = A[k];
        A[k++] = ai;
        A[i] = ak;
      }
    }
    return k;
  }
  int halfN = N / 2;
  int k1;
  k1 = _Cilk_spawn ParPartition(pivot, halfN, A);
  int k2 = ParPartition(pivot, N - halfN, A + halfN);
  _Cilk_sync;

  int minlen = (halfN - k1) < k2 ? (halfN / 2 - k1) : k2;
  
  _Cilk_for (int i = 0; i < minlen; i ++) {
    int tmp = A[i + k1];
    A[i + k1] = A[halfN + k2 - i - 1];
    A[halfN + k2 - i - 1] = tmp;
  }
  return k1 + k2;
}

void quickSort2(int N, keytype* A) {
  if (N <= 10000)
     sequentialSort(N, A);
  else {
    keytype pivot = A[rand() % N];
    int n_le = ParPartition(pivot, N, A);
    _Cilk_spawn quickSort2(n_le, A);
    quickSort2(N - n_le, A + n_le);
    _Cilk_sync;
  }
}

void
parallelSort (int N, keytype* A)
{
  if (PARALLEL_PARTITION_PREFIXSUM) {
    int* pointers = (int*)malloc(N * sizeof(int));
    int* rpointers = (int*)malloc(N * sizeof(int));
    keytype* B = (keytype*)malloc(N * sizeof(keytype));
    quickSort (N, A, pointers, rpointers, B);
    free(pointers);
    free(rpointers);
    free(B);
  } else {
    quickSort2(N, A);
  }
}

/* eof */
